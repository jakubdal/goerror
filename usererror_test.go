package goerror

import (
	"fmt"
	"testing"

	"github.com/pkg/errors"
)

func TestUserMessage(t *testing.T) {
	tests := []struct {
		name string

		err error

		expectedMessage string
		expectedError   string
	}{
		{
			name:            "nil error",
			err:             nil,
			expectedMessage: "",
		},
		{
			name:            "wrapped nil error",
			err:             WithUserMessage(nil, "1"),
			expectedMessage: "",
		},
		{
			name:            "plain error",
			err:             fmt.Errorf("plain error"),
			expectedMessage: "",
			expectedError:   "plain error",
		},
		{
			name: "plain wrapped in UserMessage",
			err: WithUserMessage(
				fmt.Errorf("plain error"),
				"1",
			),
			expectedMessage: "1",
			expectedError:   "plain error",
		},
		{
			name: "double-wrapping UserMessage",
			err: WithUserMessage(
				WithUserMessage(
					fmt.Errorf("plain error"),
					"1",
				),
				"2",
			),
			expectedMessage: "1",
			expectedError:   "plain error",
		},
		{
			name: "UserMessage wrapping plain wrapped in plain",
			err: errors.Wrap(
				WithUserMessage(
					fmt.Errorf("plain error"),
					"1",
				),
				"wrapping error",
			),
			expectedMessage: "1",
			expectedError:   "wrapping error: plain error",
		},
		{
			name: "UserMessage wrapped in plain",
			err: errors.Wrap(
				NewUserMessageError("1"),
				"plain error",
			),
			expectedMessage: "1",
			expectedError:   "plain error: 1",
		},
		{
			name:            "UserMessage",
			err:             NewUserMessageError("1"),
			expectedMessage: "1",
			expectedError:   "1",
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			if userMessage := UserMessage(tc.err); userMessage != tc.expectedMessage {
				t.Errorf(
					"invalid userMessage\nexpected:\n\t%v\ngot:\n\t%v",
					tc.expectedMessage, userMessage,
				)
			}

			if tc.err == nil {
				return
			}

			if errMsg := tc.err.Error(); errMsg != tc.expectedError {
				t.Errorf(
					"invalid error\nexpected:\n\t%+#v\ngot:\n\t%+#v",
					tc.expectedError, errMsg,
				)
			}
		})
	}
}
