package goerror

// UserMessageError contains errors that can be displayed for user
type UserMessageError struct {
	err         error
	userMessage string
}

func (err UserMessageError) Error() string {
	if err.err == nil {
		return err.userMessage
	}
	return err.err.Error()
}

// UserMessage returns userMessage contained by given UserMessageError
func (err UserMessageError) UserMessage() string {
	return err.userMessage
}

// Cause returns the underyling cause of the error
func (err UserMessageError) Cause() error {
	return err.err
}

// UserMessager is interface that allows extraction of UserMessage from error
type UserMessager interface {
	UserMessage() string
}

// NewUserMessageError returns new UserMessageError with provided userMessage
func NewUserMessageError(userMessage string) error {
	return UserMessageError{
		userMessage: userMessage,
	}
}

// UserMessage returns message for user from error. The deepest message will be
// returned
//
// If no message is found, or error is nil, empty string will be returned
func UserMessage(err error) string {
	if err == nil {
		return ""
	}

	type causer interface {
		Cause() error
	}
	cause, ok := err.(causer)
	if ok {
		parentMessage := UserMessage(cause.Cause())
		if parentMessage != "" {
			return parentMessage
		}
	}
	messager, ok := err.(UserMessager)
	if ok {
		return messager.UserMessage()
	}
	return ""
}

// WithUserMessage return `err`, wrapped with `userMesasge` for
// `UserMessage` function reading
//
// If provided `err` is `nil`, `nil` is returned from this function
func WithUserMessage(err error, userMessage string) error {
	if err == nil {
		return nil
	}
	return UserMessageError{
		err:         err,
		userMessage: userMessage,
	}
}
