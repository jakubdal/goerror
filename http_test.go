package goerror

import (
	"fmt"
	"net/http"
	"testing"
)

func TestHTTPStatusCodeError(t *testing.T) {
	err := WithHTTPStatusCode(fmt.Errorf("expected message"), 1)
	if msg := err.Error(); msg != "expected message" {
		t.Errorf("invalid error message: %+#v", msg)
	}
}

func TestHTTPStatusCode(t *testing.T) {
	tests := []struct {
		name string

		err          error
		expectedCode int
	}{
		{
			name:         "nil error",
			err:          nil,
			expectedCode: http.StatusOK,
		},
		{
			name:         "wrapped nil error",
			err:          WithHTTPStatusCode(nil, 1),
			expectedCode: http.StatusOK,
		},
		{
			name:         "plain error",
			err:          fmt.Errorf("plain error"),
			expectedCode: http.StatusInternalServerError,
		},
		{
			name: "plain wrapped in status code",
			err: WithHTTPStatusCode(
				fmt.Errorf("plain error"),
				1,
			),
			expectedCode: 1,
		},
		{
			name: "double-wrapped status code",
			err: WithHTTPStatusCode(
				WithHTTPStatusCode(
					fmt.Errorf("plain error"),
					1,
				),
				2,
			),
			expectedCode: 1,
		},
	}

	for _, tc := range tests {
		tc := tc
		t.Run(tc.name, func(t *testing.T) {
			t.Parallel()

			if code := HTTPStatusCode(tc.err); code != tc.expectedCode {
				t.Errorf(
					"invalid code\nexpected:\n\t%v\ngot:\n\t%v",
					tc.expectedCode, code,
				)
			}
		})
	}
}
