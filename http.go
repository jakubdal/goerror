package goerror

import (
	"net/http"
)

// HTTPStatusCodeError is wrapper for error that holds
//  HTTPStatusCode information
type HTTPStatusCodeError struct {
	err        error
	statusCode int
}

func (err HTTPStatusCodeError) Error() string {
	return err.err.Error()
}

// HTTPStatusCode is used to fullfil HTTPStatusCoder interface
func (err HTTPStatusCodeError) HTTPStatusCode() int {
	return err.statusCode
}

// Cause returns the underyling cause of the error
func (err HTTPStatusCodeError) Cause() error {
	return err.err
}

// HTTPStatusCoder is interface that allows extraction of HTTP Status Code from
// error
type HTTPStatusCoder interface {
	HTTPStatusCode() int
}

// HTTPStatusCode returns HTTP status code from error. The deepest status code
// will be returned
//
// If no status code is found, `http.StatusInternalServerError` is returned. If
// `err`, or one of underyling errors is nil, `http.StatusOK` will be returned
func HTTPStatusCode(err error) int {
	if err == nil {
		return http.StatusOK
	}

	type causer interface {
		Cause() error
	}
	cause, ok := err.(causer)
	if ok {
		parentCode := HTTPStatusCode(cause.Cause())
		if parentCode != http.StatusInternalServerError {
			return parentCode
		}
	}
	coder, ok := err.(HTTPStatusCoder)
	if ok {
		return coder.HTTPStatusCode()
	}
	return http.StatusInternalServerError
}

// WithHTTPStatusCode return `err`, wrapped with `statusCode` for
// `HTTPStatusCode` function reading
//
// If provided `err` is `nil`, `nil` is returned from this function
func WithHTTPStatusCode(err error, statusCode int) error {
	if err == nil {
		return nil
	}

	return HTTPStatusCodeError{
		err:        err,
		statusCode: statusCode,
	}
}
